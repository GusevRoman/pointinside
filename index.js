var Converter = require('csvtojson').Converter;
var converter1 = new Converter({});
var converter2 = new Converter({});
var async = require('async');
var _ = require('lodash');
var fs = require('fs');
var logs, events, index;
var matched = [];


var getLogEntries = function(callback) {
  fs.createReadStream('./logEntries.csv').pipe(converter1);
  converter1.on('end_parsed', function (jsonArray) {
    logs = jsonArray;
    callback(null);
  });
};

var getRegisteredEvents = function(callback) {
  fs.createReadStream('./registeredEvents.csv').pipe(converter2);
  converter2.on('end_parsed', function (jsonArray) {
    events = jsonArray;
    callback(null, 'done');
  });
};

async.waterfall([getLogEntries, getRegisteredEvents], function(err, result) {
  console.log(match(logs, events));
});

var match = function(logs, events) {
  //Declare the returning object
  var objToReturn = [];
  objToReturn.unmatchedLogEntries = [];

  //Iterate through logs array
  _.forEach(logs, function(log) {
    //Filter all events by particular log
    matched = _.filter(events, {'applicationType': log.applicationType, 'resourceType': log.resourceType, 'resourceName': log.resourceName});
    //Check if there is match
    if (matched.length == 0) {
      //If no match push it to unmatchedList
      objToReturn.unmatchedLogEntries.push(log);
    } else {
      //If there is a match - check if this event already exists in objToReturn
      index = _.findIndex(objToReturn, {'eventName': matched[0].eventName});
      if (index == -1) {
        //Push this event with count = 1
        objToReturn.push({'eventName': matched[0].eventName, 'count': 1});
      } else {
        //Increment count
        objToReturn[index].count++;
      }
    }
  });

  return objToReturn;
};
